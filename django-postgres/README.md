# Command History from Bare Metal

```console
sudo ufw allow 8000:8100/tcp
sudo ufw default allow outgoing
# sudo ufw default allow incoming

wget http://172.17.1.1:8080
wget http://192.168.1.60:8080
wget http://localhost:8080

ls -al
mkdir -p ~/code/tangibleai
cd ~/code/tangibleai/
git clone git@gitlab.com:tangibleai/qary
cd qary
ls -al
# more .gitlab-ci.yml 
```

Install miniconda:

```console
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
~/miniconda3/bin/conda init bash
exit
```

Install qary:

```
cd ~/code/tangibleai/qary
more README.md
conda update -y -n base -c defaults conda
conda create -y -n qaryenv 'python>=3.6.5,<3.9'
conda env update -n qaryenv -f environment.yml
conda activate qaryenv || source activate qaryenv
pip install --editable .
```

Docker compose example from Docker docs:

```console
cd ~/code/tangibleai/docker-examples/
ls -al
mkdir django-postgres
cd django-postgres
echo "
FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/"

nano Dockerfile
nano requirements.txt
nano docker-compose.yml

sudo apt install curl
sudo curl -L https://github.com/docker/compose/releases/download/1.28.2/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
docker ps
docker-compose run web django-admin startproject composeexample .
```

```
which docker
ls -al /usr/bin/docker
sudo chown -R $USER:$USER .
nano composeexample/settings.py 
cd ~/code/tangibleai/docker-examples
git init
git add django-postgres/
cd django-postgres/
ls -al
git config --global user.email "hobson@totalgood.com"
git config --global user.name "Hobson Lane"
git status
git add .
git commit -am 'initial commit of working django-postgres docker-compose project'
git remote add origin git@gitlab.com:tangibleai/docker-examples.git
git push -u origin master
```

Build and power on:

```console
docker-compose up --build
nano composeexample/settings.py 
docker-compose up --build
nano Dockerfile 
python manage.py migrate
nano requirements.txt
nano Dockerfile 
nano docker-compose.yml 
docker-compose up --build
```

Need an entrypoint.sh file that waits for postgres db to come up first and do migrations only then.

```
nano entrypoint.sh
nano Dockerfile
docker-compose up --build
nano composeexample/settings.py 
git commit -am 'everything working except qary.ai url and ssl'
git push
docker ps
```

Enable ssh on the GPU.

```console
sudo apt install openssh-server
sudo systemctl status ssh
sudo nano /etc/ssh/sshd_config
# this test worked
# ssh 127.0.0.1
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo apt install docker.io
sudo systemctl enable --now docker

sudo groupadd docker
sudo usermod -aG docker $USER
groups

sudo ufw default allow outgoing

# to find lan IP address
ifconfig

mkdir code
cd code
mkdir docker-examples
cd docker-examples
sudo ufw logging on
sudo ufw enable
sudo ufw status numbered

sudo ufw app list
sudo ufw allow https
sudo ufw allow http
sudo ufw allow 8000/tcp

# laptop on LAN
sudo ufw allow from 192.168.1.67  

docker ps
curl ifconfig.me
curl ifconfig.me/all
ping 24.116.156.5
ping qary.ai
ping tanbot.qary.ai
ping qary.ai
ping www.qary.ai

docker ps
docker kill 5be0001477e5

docker ps
docker exec -it f1e73f3764d3 python manage.py migrate
docker exec -it f1e73f3764d3 python manage.py createsuperuser
```
