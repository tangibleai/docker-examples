# Example `Docerfile`s and `docker-compse.yml`s

## django-postgres

- based on [example](https://docs.docker.com/machine/examples/ocean/) in Docker documentation
- boilerplate django project with Postgres database
- entrypoint.sh sh to automatically keep migrations up to speed

## torchserve

- based on the official pytorch [torchserve container](https://hub.docker.com/r/pytorch/torchserve)
- DOES NOT WORK

## django-nginx-postgres

- based on 

## sonicdb

- based on [sonic README](https://github.com/valeriansaliou/sonic) and [blog](https://journal.valeriansaliou.name/announcing-sonic-a-super-light-alternative-to-elasticsearch/)
- rust
- full text search db

